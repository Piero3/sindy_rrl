from pysindy.optimizers import STLSQ
from pysindy.feature_library import PolynomialLibrary
from pysindy import FiniteDifference
import plotly.express as px
from sklearn.linear_model import Lasso
import pandas as pd
import numpy as np

class Light_Curve:
    def __init__(self, name):
        self.name = name
        self.phase = []
        self.df = []

    def check_class(self):
        print('The name of the star is ' + self.name)

    def find_phase(self):
        with open(self.name) as f:
            self.first_line = f.readline()
            self.phase = float(self.first_line[18:-18])
            return self.phase

    def find_df(self):
        self.columns_name = ['HJD', 'phase', 'mag', 'errmag', 'AP0_step']
        self.df = pd.read_csv(self.name,index_col=False, header=0, names = ['HJD'], comment='#')
        self.df = pd.DataFrame(self.df['HJD'].str.split().to_list())
        if len(self.df.columns)==5:
            self.df.columns = self.columns_name
            self.df = self.df.astype({'HJD' : np.float64, 'phase' : np.float64, 'mag' : np.float64, 'errmag' : np.float64, 'AP0_step' : np.str})

        if len(self.df.columns)==4:
            self.df.columns = self.columns_name[:-1]
            self.df = self.df.astype({'HJD' : np.float64, 'phase' : np.float64, 'mag' : np.float64, 'errmag' : np.float64})#, 'AP0_step' : np.str})
        return self.df


    # For find name, filter, subset of the VBIRUJHK new catalogue
    def find_name_filter_subset(self):
        filters = ['V', 'B', 'I', 'R', 'U', 'J', 'H', 'K']
        numlist = list(range(1,9))
        regex_filter = re.compile(r'_(\d)_') #any digit between _ and _
        regex_name = re.compile(r'(?<=allcvs\/)[^_]*') #everything before _ and everything after allcvs/
        regex_subset = re.compile(r'(_\d_)(.*)(?=.fas)')

        match_filter = regex_filter.search(self.name)
        self.filter = match_filter.group(1)
        self.filter = filters[numlist.index(int(self.filter))] # convert the number in filter name
        match_subset = regex_subset.search(self.name)
        self.subset = match_subset.group(2)
        match_name = regex_name.search(self.name)
        self.name =  match_name.group(0)
        return match_name.group(0), match_filter.group(1), match_subset.group(2)


    def add_name_filter_subset(self):
        self.df['star'] = '{0}'.format(self.name)
        self.df['filter'] = np.str(self.filter)
        self.df['subset'] = np.str(self.subset)
        self.df = self.df.astype({'star' : np.str, 'filter' : np.str, 'subset' : np.str})

    def plot_lcv(self):
        if self.df == []:
            self.find_df()
            fig = px.scatter(self.df, x='phase', y='mag')
            fig.show()
        else:
            fig = px.scatter(self.df, x='phase', y='mag')
            fig.show()

    def sort_values(self, column):
        self.find_df()
        self.df = self.df.sort_values(by=column)

    def drop_duplicates(self, column):
        self.df = pd.DataFrame.drop_duplicates(self.df, subset=column)

        ### Sorto e droppo i duplicati per evitare problemi con sindy

    def sort_and_drop(self, column):
        self.find_df()
        self.sort_values(column)
        self.drop_duplicates(column)

    def repeat_phase(self, iteration):
        self.sort_and_drop('phase')
        df1, df2 = self.df, pd.DataFrame.copy(self.df)
        for i in range(1, iteration):
            #print('phase :', i)
            df2['phase'] += i
            df1, df2 = pd.concat([df1, df2]), pd.concat([df1, df2])
        self.df = df2
        return df2

    def interpolate(self, n_phase = 1, n_points=1000):
        if self.df == []:
            self.sort_and_drop('phase')
        else:
            pass
        self.tck = interpolate.splrep(self.df.phase, self.df.mag, k=3, s=1)
        self.t = np.linspace(0, 1*n_phase, n_points)
        self.x = interpolate.splev(self.t, self.tck, der=0)
        self.x_dot = interpolate.splev(self.t, self.tck, der=1)
        self.X = np.array([self.x, self.x_dot]).reshape((self.x.shape[0], 2))

        return self.X
#        return pd.DataFrame({'t': self.t, 'x' : self.x, 'xdot': self.x_dot})


    def sindy_spline(self, n_phase=1, n_points=1000):

        spline_values = self.interpolate(n_phase, n_points)
        t = np.linspace(0, 1*n_phase, n_points)
        x = spline_values[:,0]
        xdot = spline_values[:,1]
        X = spline_values

        lasso_optimizer = Lasso(alpha=0.2, max_iter=2000, fit_intercept=False)

        model = ps.SINDy(feature_names=['x', 'xdot'], differentiation_method=ps.SmoothedFiniteDifference(order=2),
                         optimizer=lasso_optimizer, feature_library=PolynomialLibrary(degree=2))

        model.fit(X, t)
        model.print()

        #return spline_values
        #model.fit(interpolate(n_phase, n_points))

    def sindy_time(self, iteration, feature_names, differentiation_method=FiniteDifference,
                   optimizer=STLSQ(alpha=0.05),
                   feature_library=PolynomialLibrary(degree=2)):

        model = ps.SINDy(feature_names=feature_names, differentiation_method=differentiation_method,
                         optimizer=optimizer,
                         feature_library=feature_library)


        model.fit(np.array(self.repeat_phase(iteration).mag), np.array(self.repeat_phase(iteration).phase))
#        model.print()
        return model
